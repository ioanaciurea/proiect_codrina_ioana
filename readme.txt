Library Management

This project consists of a school library management system. The system have two types of users:
1. Administrator users - allowed to manage the users of the library
2. Librarian users - allowed to manage the contents of the library

a) User management
The administrator users cannot update the items of the library and the librarian users cannot update the users of the system.

Initially the system is coming with one default administrator user (with a known name/password).

User details include: user name, password, real name, email.

Administrator users are allowed to perform these user-related operations:
1. Define new administrator or librarian users
2. View existing users in the system (support also searching)
3. Update details of existing users (password, email etc.)
4. Delete administrator or librarian users. The last ramaining administrator user cannot be deleted.

b) Item management
The library contains physical items such as: books, magazines, articles, various digital storage media (CDs, DVDs).

Librarian users are allowed to perform these item-related operations:
1. Add new items to the library
2. View and search the contents of the library
3. Update details of existing items (author name, title, singer etc.)
4. Mark an item as "lent" with a set return date
5. Mark an item as available when it is returned
6. Retire an item from the library (should never delete it), by marking it as "lost".

Default administrator user name: admin
                      password: 1234