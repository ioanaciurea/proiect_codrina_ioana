package wantsome.project.presentation.controller.user_controller;

import org.mindrot.jbcrypt.BCrypt;
import spark.Request;
import spark.Response;
import wantsome.project.database.dao.UserDao;
import wantsome.project.presentation.controller.SparkUtil;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.halt;

public class LoginController {


    public static Object mainPageLogin(String error, String username, String password) {
        Map<String, Object> model = new HashMap<>();
        model.put("error", error);
        model.put("username", username);
        model.put("password", password);

        return SparkUtil.render(model, "mainPageLogin.vm");
    }

    public static String getCurrentUsername(Request req, Response res) {
        String username = req.session().attribute("username");
        if (username == null) {
            res.redirect("/login");
            halt();
        }
        return username;
    }


    static boolean sessionValidation(Request req, Response res, String currentUser) {
        currentUser = getCurrentUsername(req, res);

        if (req.session(false) == null) {
            return false;
        }

        return currentUser != null && req.session(false).attribute("admin") != null;
    }

    static boolean isAdmin(Request req, Response res, String currentUser) {
        return Boolean.parseBoolean(req.session(false).attribute("admin"));
    }


    public static Object verifyCredentials(Request req, Response res) {
        String username = req.queryParams("username");
        UserDao userDao = new UserDao();
        String inputPassword = req.queryParams("password");

        if (!userDao.getByLoginName(username).isPresent()) {
            return mainPageLogin("Wrong username or password. Please try again", "", "");
        } else if (userDao.getByLoginName(username).get().getPassword()
                .equals(BCrypt.hashpw(inputPassword, userDao.getByLoginName(username).get().getHashSalt()))) {
            req.session(true).attribute("username", username);
            if ("ADMINISTRATOR".equals(userDao.getByLoginName(username).get().getType().name())) {
                req.session().attribute("admin", true);
                res.redirect("/mainAdmin");
                return res;
            } else if ("LIBRARIAN".equals(userDao.getByLoginName(username).get().getType().name())) {
                req.session().attribute("admin", false);
                res.redirect("/main");
                return res;
            } else {
                return mainPageLogin("Access is denied. You may not have the appropriate permissions for access.", "", "");
            }
        } else {
            return mainPageLogin("Access is denied. You may not have the appropriate permissions for access.", "", "");
        }
    }

    public static Object handleLogoutPost(Request req, Response res) {
        req.session(false).invalidate();
        res.redirect("/login");
        return null;
    }
}
