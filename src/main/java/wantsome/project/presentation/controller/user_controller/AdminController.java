package wantsome.project.presentation.controller.user_controller;

import org.mindrot.jbcrypt.BCrypt;
import spark.Request;
import spark.Response;
import wantsome.project.database.dao.UserDao;
import wantsome.project.database.dto.users.User;
import wantsome.project.database.dto.users.UserType;
import wantsome.project.presentation.controller.SparkUtil;

import java.util.*;

import static java.util.stream.Collectors.toList;

public class AdminController {
    private static final UserDao userDao = new UserDao();
    private static String sortBy = "ID";
    private static boolean sortAsc = true;


    public static Object adminMenuPage(Request req, Response res, String error) {

        String username = LoginController.getCurrentUsername(req, res);
        readSortParams(req);
        getWordToSearchFromParamOrSes(req);
        Map<String, Object> model = new HashMap<>();
        model.put("allUsers", getSortedUsers(handleSearchRequest(req, res)));
        model.put("sortBy", sortBy);
        model.put("sortAsc", sortAsc);
        model.put("searchBy", req.queryParamOrDefault("searchBy", ""));
        model.put("wordToSearch", req.queryParamOrDefault("wordToSearch", ""));
        model.put("error", error);
        return SparkUtil.render(model, "adminMenuPage.vm");
    }


    public static List<User> handleSearchRequest(Request req, Response res) {
        String username = LoginController.getCurrentUsername(req, res);
        List<User> list = new ArrayList<>();
        String wordToSearch = req.queryParams("wordToSearch");
        String searchBy = req.queryParams("searchBy");
        try {
            if (wordToSearch != null && !wordToSearch.isEmpty()) {
                if (searchBy == null || searchBy.isEmpty()) {
                    System.out.println("select category fist");
                    return new ArrayList<>();
                }
                switch (searchBy) {
                    case "Username":
                        list.add(userDao.getByLoginName(wordToSearch).get());
                        break;
                    case "Type":
                        list.addAll(userDao.getByType(UserType.valueOf(wordToSearch.toUpperCase())));
                        break;
                    case "Id":
                        list.add(userDao.get(Integer.valueOf(wordToSearch)).get());
                        break;
                    default:
                        System.out.println("No user");
                }
            } else {
                list.addAll(userDao.getAll());
            }
        } catch (Exception e) {
            System.out.println("error handle search" + e.getMessage());
        }
        return list;
    }

    private static void readSortParams(Request req) {
        String newSortBy = req.queryParams("sortBy");
        if (newSortBy != null) {
            sortAsc = newSortBy.equals(sortBy) ? !sortAsc : true;
            sortBy = newSortBy;
        }
    }


    private static List<User> getSortedUsers(List<User> list) {
        Comparator<User> comparator;
        switch (sortBy) {
            case "Username":
                comparator = Comparator.comparing(User::getLoginName);
                break;
            case "Real_name":
                comparator = Comparator.comparing(User::getRealName);
                break;
            case "Email":
                comparator = Comparator.comparing(User::getEmail);
                break;
            case "Type":
                comparator = Comparator.comparing(User::getType);
                break;
            default:
                comparator = Comparator.comparing(User::getId);
                break;
        }
        if (!sortAsc) {
            comparator = comparator.reversed();
        }
        return list.stream().sorted(comparator).collect(toList());
    }


    private static String getWordToSearchFromParamOrSes(Request req) {
        String param = req.queryParams("wordToSearch"); //read it from current request (if sent)
        if (param != null) {
            req.session().attribute("wordToSearch", param); //save it to session for later
        } else {
            param = req.session().attribute("wordToSearch"); //try to read it from session
        }
        return param;
    }

    public static Object handleUserSearchRequest(Request req, Response res) {
        String wordToSearch = req.queryParams("wordToSearch");
        return adminMenuPage(req, res, "");
    }

    public static Object handleUserDeleteRequest(Request req, Response res) {
        String id = req.params("id");
        if (!(userDao.getByLoginName(LoginController.getCurrentUsername(req, res)).get().getId() == Integer.parseInt(id))) {
            try {
                userDao.delete(Integer.valueOf(id));
            } catch (Exception e) {
                System.out.println("Error deleting user with id '" + id + "': " + e.getMessage());
                return adminMenuPage(req, res, "Error deleting user with id '" + id + "': " + e.getMessage());
            }
        } else {
            return adminMenuPage(req, res, "Error deleting user with id " + id + ": cannot delete current user ");
        }
        res.redirect("/mainAdmin");
        return res;
    }

    public static String showAddUpdateUserForm(Request req, Response res) {
        String currentUsername = LoginController.getCurrentUsername(req, res);
        String id = req.params("id");
        boolean isUpdate = id != null && !id.isEmpty();

        Optional<User> optUser = Optional.empty();
        if (isUpdate) {
            try {
                optUser = userDao.get(Integer.parseInt(id));
            } catch (Exception e) {
                System.err.println("Error loading user with id '" + id + "': " + e.getMessage());
            }
            if (!optUser.isPresent()) {
                return "Error: user " + id + " not found!";
            }
        }

        return renderAddUpdateForm(
                optUser.map(i -> String.valueOf(i.getId())).orElse(""),
                optUser.map(User::getLoginName).orElse(""),
                optUser.map(User::getPassword).orElse(""),
                optUser.map(User::getRealName).orElse(""),
                optUser.map(User::getEmail).orElse(""),
                optUser.map(i -> i.getType().name()).orElse(""),
                "");
    }

    private static String renderAddUpdateForm(String id, String username, String password,
                                              String realName, String email, String type, String errorMessage) {
        Map<String, Object> model = new HashMap<>();
        model.put("prevId", id);
        model.put("prevLoginName", username);
        model.put("prevPassword", password);
        model.put("prevRealName", realName);
        model.put("prevEmail", email);
        model.put("prevType", type);
        model.put("errorMsg", errorMessage);
        model.put("isUpdate", id != null && !id.isEmpty());

        return SparkUtil.render(model, "addUser.vm");
    }

    public static Object handleAddUpdateUserRequest(Request req, Response res) {
        //read form values (posted as params)
        String id = req.queryParams("id");
        String username = req.queryParams("username");
        String password = req.queryParams("password");
        String realName = req.queryParams("realName");
        String email = req.queryParams("email");
        String type = req.queryParams("type");

        boolean isUpdate = id != null && !id.isEmpty();

        try {
            User user = validateAndBuildUser(id, username, password, realName, email, type, req, res);

            if (isUpdate) {
                userDao.update(user);
            } else {
                userDao.insert(user);
            }

            res.redirect("/mainAdmin");
            return res;

        } catch (Exception e) {
            return renderAddUpdateForm(id, username, password, realName, email, type, e.getMessage());
        }
    }

    public static User validateAndBuildUser(String id, String username, String password, String realName, String email, String type, Request req, Response res) {

        String hashedSalt = BCrypt.gensalt();
        String hashedPassword = BCrypt.hashpw(password, hashedSalt);

        if (username == null || username.trim().isEmpty()) {
            throw new RuntimeException("Username is required!");
        }

        if (password == null || password.trim().isEmpty()) {
            throw new RuntimeException("Password is required!");
        }

        if (realName == null || realName.trim().isEmpty()) {
            throw new RuntimeException("Real name is required!");
        }
        if (email == null || email.trim().isEmpty()) {
            throw new RuntimeException("Email is required!");
        }
        UserType userType;
        try {
            userType = UserType.valueOf(type);
        } catch (Exception e) {
            throw new RuntimeException("Invalid type value: '" + type +
                    "', must be one of: " + Arrays.toString(UserType.values()));
        }
        if ((id != null && !id.isEmpty()) && (password.equals(userDao.getByLoginName(username).get().getPassword()))) {
            return new User(username, userDao.getByLoginName(username).get().getHashSalt(), userDao.getByLoginName(username).get().getPassword(), realName, email, userType);
        }
        return new User(username, hashedSalt, hashedPassword, realName, email, userType);
    }

}
