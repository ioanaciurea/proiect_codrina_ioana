package wantsome.project.presentation.controller.library_controller;

import spark.Request;
import spark.Response;
import wantsome.project.database.dao.LibraryDao;
import wantsome.project.database.dto.library.*;
import wantsome.project.presentation.controller.user_controller.LoginController;

import java.sql.Date;
import java.util.Optional;

public class AddUpdateItem {
    private static final LibraryDao libraryDao = new LibraryDao();

    public static String showAddUpdateItemForm(Request req, Response res) {
        String currentUsername = LoginController.getCurrentUsername(req, res);
        String id = req.params("id");
        boolean isUpdateItem = id != null && !id.isEmpty() && !id.equals(String.valueOf(0));

        Optional<MediaEntity> optItem = Optional.empty();
        String mediaType = req.queryParams("mediaType");
        if (isUpdateItem) {
            try {
                optItem = libraryDao.get(Integer.parseInt(id));
                mediaType = String.valueOf(optItem.get().getType());
            } catch (Exception e) {
                System.err.println("Error loading item with id " + id + ": " + e.getMessage());
            }
            if (!optItem.isPresent()) {
                return "Error: item " + id + " not found!";
            }
        }


        if (mediaType.equalsIgnoreCase(String.valueOf(MediaType.ARTICLE))) {
            return RenderAddUpdateItems.renderAddUpdateArticle(
                    optItem.map(item -> ((Article) item)).orElse(new Article("", "", "")),
                    "", req);
        }
        if (mediaType.equalsIgnoreCase(String.valueOf(MediaType.BOOK))) {
            return RenderAddUpdateItems.renderAddUpdateBook(
                    optItem.map(item -> ((Book) item)).orElse(new Book("", "", "")),
                    "", req);
        }
        if (mediaType.equalsIgnoreCase(String.valueOf(MediaType.CD))) {
            return RenderAddUpdateItems.renderAddUpdateCD(
                    optItem.map(item -> ((CD) item)).orElse(new CD("", "", "")),
                    "", req);
        }
        if (mediaType.equalsIgnoreCase(String.valueOf(MediaType.DVD))) {
            return RenderAddUpdateItems.renderAddUpdateDVD(
                    optItem.map(item -> ((DVD) item)).orElse(new DVD("", 0, false)),
                    "", req);
        }
        if (mediaType.equalsIgnoreCase(String.valueOf(MediaType.MAGAZINE))) {
            return RenderAddUpdateItems.renderAddUpdateMagazine(
                    optItem.map(item -> ((Magazine) item)).orElse(new Magazine("", null, "")),
                    "", req);
        }
        return "";
    }

    public static Object handleAddUpdateItemRequest(Request req, Response res) {
        String id = req.queryParams("id");
        MediaType mediaType = MediaType.valueOf(req.queryParams("Type"));
        String title = req.queryParams("title");
        String startDate = req.queryParams("startDate");
        String dueDate = req.queryParams("dueDate");
        String itemState = req.queryParams("itemState");
        boolean isUpdateItem = id != null && !id.isEmpty() && !id.equals(String.valueOf(0));

        Optional<MediaEntity> item = Optional.empty();
        String author;
        String publisher;
        switch (mediaType) {
            case ARTICLE:
                author = req.queryParams("author");
                String articleSource = req.queryParams("articleSource");
                try {
                    item = validateAndBuildItem(id, MediaType.ARTICLE, title, author, "", articleSource, "", "", 0, false, "", startDate, dueDate, itemState);
                    if (isUpdateItem && item.isPresent()) {
                        libraryDao.update(item.get());
                        LibraryDao.updateState(item.get());
                    } else {
                        item.ifPresent(libraryDao::insert);
                    }
                    res.redirect("/main");
                    return res;
                } catch (Exception e) {
                    return RenderAddUpdateItems.renderAddUpdateArticle((Article) item.get(), e.getMessage(), req);
                }
            case BOOK:
                publisher = req.queryParams("publisher");
                author = req.queryParams("author");
                try {
                    item = validateAndBuildItem(id, MediaType.BOOK, title, author, publisher, "", "", "", 0, false, "", startDate, dueDate, itemState);
                    if (isUpdateItem && item.isPresent()) {
                        libraryDao.update(item.get());
                        LibraryDao.updateState(item.get());
                    } else {
                        item.ifPresent(libraryDao::insert);
                    }
                    res.redirect("/main");
                    return res;
                } catch (Exception e) {
                    return RenderAddUpdateItems.renderAddUpdateBook((Book) item.get(), e.getMessage(), req);
                }
            case CD:
                String singer = req.queryParams("singer");
                String album = req.queryParams("album");
                try {
                    item = validateAndBuildItem(id, MediaType.CD, title, "", "", "", singer, album, 0, false, "", startDate, dueDate, itemState);
                    if (isUpdateItem && item.isPresent()) {
                        libraryDao.update(item.get());
                        LibraryDao.updateState(item.get());
                    } else {
                        item.ifPresent(libraryDao::insert);
                    }
                    res.redirect("/main");
                    return res;
                } catch (Exception e) {
                    return RenderAddUpdateItems.renderAddUpdateCD((CD) item.get(), e.getMessage(), req);
                }
            case DVD:
                String duration = req.queryParams("duration");
                String fullHD = req.queryParams("fullHD");
                try {
                    item = validateAndBuildItem(id, MediaType.DVD, title, "", "", "", "", "", Integer.parseInt(duration), Boolean.parseBoolean(fullHD), "", startDate, dueDate, itemState);
                    if (isUpdateItem && item.isPresent()) {
                        libraryDao.update(item.get());
                        LibraryDao.updateState(item.get());
                    } else {
                        item.ifPresent(libraryDao::insert);
                    }
                    res.redirect("/main");
                    return res;
                } catch (Exception e) {
                    return RenderAddUpdateItems.renderAddUpdateDVD((DVD) item.get(), e.getMessage(), req);
                }
            case MAGAZINE:
                String dateOfPublication = req.queryParams("dateOfPublication");
                publisher = req.queryParams("publisher");
                try {
                    item = validateAndBuildItem(id, MediaType.MAGAZINE, title, "", publisher, "", "", "", 0, false, dateOfPublication, startDate, dueDate, itemState);
                    if (isUpdateItem && item.isPresent()) {
                        libraryDao.update(item.get());
                        LibraryDao.updateState(item.get());
                    } else {
                        item.ifPresent(libraryDao::insert);
                    }
                    res.redirect("/main");
                    return res;
                } catch (Exception e) {
                    return RenderAddUpdateItems.renderAddUpdateMagazine((Magazine) item.get(), e.getMessage(), req);
                }
        }
        res.redirect("/main");
        return res;
    }


    public static Optional<MediaEntity> validateAndBuildItem(String id, MediaType mediaType, String title, String author, String publisher, String
            articleSource, String singer, String album, int duration, boolean fullHD, String dateOfPublication, String startDate, String dueDate, String itemState) {
        if (title == null || title.trim().isEmpty()) {
            throw new RuntimeException("Title is required!");
        }

        switch (mediaType) {
            case ARTICLE:
                if (author == null || author.trim().isEmpty()) {
                    throw new RuntimeException("Author is required!");
                }
                if (articleSource == null || articleSource.trim().isEmpty()) {
                    throw new RuntimeException("Article source is required!");
                }
                if (id != null && !id.isEmpty()) {
                    Article a = new Article(title, author, articleSource);
                    a.setStartDate(Date.valueOf(startDate));
                    a.setDueDate(Date.valueOf(dueDate));
                    a.setState(ItemState.valueOf(itemState));
                    return Optional.of(a);
                } else {
                    return Optional.of(new Article(title, author, articleSource));
                }
            case BOOK:
                if (author == null || author.trim().isEmpty()) {
                    throw new RuntimeException("Author is required!");
                }
                if (publisher == null || publisher.trim().isEmpty()) {
                    throw new RuntimeException("Publisher source is required!");
                }
                if (id != null && !id.isEmpty()) {
                    Book b = new Book(title, author, publisher);
                    b.setStartDate(Date.valueOf(startDate));
                    b.setDueDate(Date.valueOf(dueDate));
                    b.setState(ItemState.valueOf(itemState));
                    return Optional.of(b);
                } else {
                    return Optional.of(new Book(title, author, publisher));
                }
            case CD:
                if (singer == null || singer.trim().isEmpty()) {
                    throw new RuntimeException("Singer is required!");
                }
                if (album == null || album.trim().isEmpty()) {
                    throw new RuntimeException("Album source is required!");
                }
                if (id != null && !id.isEmpty()) {
                    CD cd = new CD(title, singer, album);
                    cd.setStartDate(Date.valueOf(startDate));
                    cd.setDueDate(Date.valueOf(dueDate));
                    cd.setState(ItemState.valueOf(itemState));
                    return Optional.of(cd);
                } else {
                    return Optional.of(new CD(title, singer, album));
                }
            case DVD:
                if (duration <= 0) {
                    throw new RuntimeException("You must enter a positive value!");
                }
                if (id != null && !id.isEmpty()) {
                    DVD dvd = new DVD(title, duration, fullHD);
                    dvd.setStartDate(Date.valueOf(startDate));
                    dvd.setDueDate(Date.valueOf(dueDate));
                    dvd.setState(ItemState.valueOf(itemState));
                    return Optional.of(dvd);
                } else {
                    return Optional.of(new DVD(title, duration, fullHD));
                }
            case MAGAZINE:
                if (publisher == null || publisher.trim().isEmpty()) {
                    throw new RuntimeException("Publisher is required!");
                }
                if (dateOfPublication == null) {
                    throw new RuntimeException("Date of publication is required!");
                }
                break;
        }
        if (id != null && !id.isEmpty()) {
            Magazine m = new Magazine(title, Date.valueOf(dateOfPublication), publisher);
            m.setStartDate(Date.valueOf(startDate));
            m.setDueDate(Date.valueOf(dueDate));
            m.setState(ItemState.valueOf(itemState));
            return Optional.of(m);
        }
        return Optional.of(new Magazine(title, Date.valueOf(dateOfPublication), publisher));
    }
}
