package wantsome.project.presentation.controller.library_controller;

import spark.Request;
import wantsome.project.database.dto.library.*;
import wantsome.project.presentation.controller.SparkUtil;

import java.util.HashMap;
import java.util.Map;

public class RenderAddUpdateItems {

    protected static String renderAddUpdateArticle(Article a, String errorMsg, Request req) {
        Map<String, Object> model = new HashMap<>();
        String id = String.valueOf(a.getMediaId());
        model.put("prevId", id);
        model.put("prevType", String.valueOf(a.getType()));
        model.put("mediaType", req.queryParamOrDefault("mediaType", ""));
        model.put("prevTitle", a.getTitle());
        model.put("prevStartDate", String.valueOf(a.getStartDate()));
        model.put("prevDueDate", String.valueOf(a.getDueDate()));
        model.put("prevState", String.valueOf(a.getState()));
        model.put("prevAuthor", a.getAuthor());
        model.put("prevSource", a.getSource());
        model.put("errorMsg", errorMsg);
        model.put("isUpdateItem", id != null && !id.isEmpty() && !id.equals(String.valueOf(0)));

        return SparkUtil.render(model, "addUpdateArticle.vm");
    }

    protected static String renderAddUpdateBook(Book b, String errorMsg, Request req) {
        Map<String, Object> model = new HashMap<>();
        String id = String.valueOf(b.getMediaId());
        model.put("prevId", id);
        model.put("prevType", String.valueOf(b.getType()));
        model.put("mediaType", req.queryParamOrDefault("mediaType", ""));
        model.put("prevTitle", b.getTitle());
        model.put("prevStartDate", String.valueOf(b.getStartDate()));
        model.put("prevDueDate", String.valueOf(b.getDueDate()));
        model.put("prevState", String.valueOf(b.getState()));
        model.put("prevAuthor", b.getAuthor());
        model.put("prevPublisher", b.getPublisher());
        model.put("errorMsg", errorMsg);
        model.put("isUpdateItem", id != null && !id.isEmpty() && !id.equals(String.valueOf(0)));

        return SparkUtil.render(model, "addUpdateBook.vm");
    }

    protected static String renderAddUpdateCD(CD cd, String errorMsg, Request req) {
        Map<String, Object> model = new HashMap<>();
        String id = String.valueOf(cd.getMediaId());
        model.put("prevId", id);
        model.put("prevType", String.valueOf(cd.getType()));
        model.put("mediaType", req.queryParamOrDefault("mediaType", ""));
        model.put("prevTitle", cd.getTitle());
        model.put("prevStartDate", String.valueOf(cd.getStartDate()));
        model.put("prevDueDate", String.valueOf(cd.getDueDate()));
        model.put("prevState", String.valueOf(cd.getState()));
        model.put("prevSinger", cd.getSinger());
        model.put("prevAlbum", cd.getAlbum());
        model.put("errorMsg", errorMsg);
        model.put("isUpdateItem", id != null && !id.isEmpty() && !id.equals(String.valueOf(0)));

        return SparkUtil.render(model, "addUpdateCD.vm");
    }

    protected static String renderAddUpdateDVD(DVD dvd, String errorMsg, Request req) {
        Map<String, Object> model = new HashMap<>();
        String id = String.valueOf(dvd.getMediaId());
        model.put("prevId", id);
        model.put("prevType", String.valueOf(dvd.getType()));
        model.put("mediaType", req.queryParamOrDefault("mediaType", ""));
        model.put("prevTitle", dvd.getTitle());
        model.put("prevStartDate", String.valueOf(dvd.getStartDate()));
        model.put("prevDueDate", String.valueOf(dvd.getDueDate()));
        model.put("prevState", String.valueOf(dvd.getState()));
        model.put("prevDuration", dvd.getDuration());
        model.put("prevFullHD", dvd.isFullHD());
        model.put("errorMsg", errorMsg);
        model.put("isUpdateItem", id != null && !id.isEmpty() && !id.equals(String.valueOf(0)));

        return SparkUtil.render(model, "addUpdateDVD.vm");
    }

    protected static String renderAddUpdateMagazine(Magazine m, String errorMsg, Request req) {
        Map<String, Object> model = new HashMap<>();
        String id = String.valueOf(m.getMediaId());
        model.put("prevId", id);
        model.put("prevType", String.valueOf(m.getType()));
        model.put("mediaType", req.queryParamOrDefault("mediaType", ""));
        model.put("prevTitle", m.getTitle());
        model.put("prevStartDate", String.valueOf(m.getStartDate()));
        model.put("prevDueDate", String.valueOf(m.getDueDate()));
        model.put("prevState", String.valueOf(m.getState()));
        model.put("prevDateOfPublication", m.getDateOfPublication());
        model.put("prevPublisher", m.getPublisher());
        model.put("errorMsg", errorMsg);
        model.put("isUpdateItem", id != null && !id.isEmpty() && !id.equals(String.valueOf(0)));

        return SparkUtil.render(model, "addUpdateMagazine.vm");
    }
}
