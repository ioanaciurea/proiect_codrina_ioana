package wantsome.project.presentation.controller.library_controller;

import spark.Request;
import spark.Response;
import wantsome.project.database.dao.LibraryDao;
import wantsome.project.database.dto.library.*;
import wantsome.project.presentation.controller.SparkUtil;
import wantsome.project.presentation.controller.user_controller.LoginController;

import java.util.*;

import static java.util.stream.Collectors.toList;

public class LibraryController {

    private static final LibraryDao libraryDao = new LibraryDao();
    private static String sortBy = "ID";
    private static boolean sortAsc = true;


    public static Object libraryMenuPage(Request req, Response res) {
        String username = LoginController.getCurrentUsername(req, res);
        readSortParams(req);

        Map<String, Object> model = new HashMap<>();
        model.put("allItems", getSortedItems(handleSearchRequest(req, res)));
        model.put("sortBy", sortBy);
        model.put("sortAsc", sortAsc);
        model.put("searchBy", req.queryParamOrDefault("searchBy", ""));
        model.put("wordToSearch", req.queryParamOrDefault("wordToSearch", ""));

        // model.put("id", req.queryParamOrDefault("id",""));
        return SparkUtil.render(model, "libraryMenuPage.vm");
    }

    private static void readSortParams(Request req) {
        String newSortBy = req.queryParams("sortBy");
        if (newSortBy != null) {
            sortAsc = newSortBy.equals(sortBy) ? !sortAsc : true;
            sortBy = newSortBy;
        }
    }

    private static List<MediaEntity> getSortedItems(List<MediaEntity> list) {
        Comparator<MediaEntity> comparator;
        switch (sortBy) {
            case "Type":
                comparator = Comparator.comparing(MediaEntity::getType);
                break;
            case "Title":
                comparator = Comparator.comparing(MediaEntity::getTitle);
                break;
            case "State":
                comparator = Comparator.comparing(MediaEntity::getState);
                break;
            default:
                comparator = Comparator.comparing(MediaEntity::getMediaId);
                break;
        }
        if (!sortAsc) {
            comparator = comparator.reversed();
        }
        return list.stream().sorted(comparator).collect(toList());
    }


    public static List<MediaEntity> handleSearchRequest(Request req, Response res) {
        String username = LoginController.getCurrentUsername(req, res);
        List<MediaEntity> list = new ArrayList<>();
        String wordToSearch = req.queryParams("wordToSearch");
        String searchBy = req.queryParams("searchBy");
        try {
            if (wordToSearch != null && !wordToSearch.isEmpty()) {
                if (searchBy == null || searchBy.isEmpty()) {
                    System.out.println("select category fist");
                    return new ArrayList<>();
                }
                switch (searchBy) {
                    case "ID":
                        list.add(libraryDao.get(Integer.parseInt(wordToSearch)).get());
                        break;
                    case "Title":
                        list.addAll(libraryDao.selectByTitleContains(wordToSearch));
                        break;
                    case "State":
                        list.addAll(libraryDao.selectByState(ItemState.valueOf(wordToSearch)));
                        break;
                    default:
                        System.out.println("No item");
                }
            } else {
                list.addAll(libraryDao.getAll());
            }
        } catch (Exception e) {
            System.out.println("error handle search" + e.getMessage());
        }
        return list;
    }

    public static Object handleItemDetails(Request req, Response res) {
        String username = LoginController.getCurrentUsername(req, res);
        String id = req.params("id");
        String mediaType = "";
        Map<String, Object> model = new HashMap<>();

        Optional<MediaEntity> optItem = Optional.empty();
        try {
            optItem = libraryDao.get(Integer.parseInt(id));
            mediaType = String.valueOf(optItem.get().getType());
            model.put("id", id);
            model.put("mediaType", mediaType);
            model.put("title", optItem.get().getTitle());
            model.put("state", optItem.get().getState());
            model.put("startDate", optItem.get().getStartDate());
            model.put("dueDate", optItem.get().getDueDate());
            //model.put("studendId", optItem.get().getStudentId());
        } catch (Exception e) {
            System.err.println("Error loading item with id " + id + ": " + e.getMessage());
        }
        if (!optItem.isPresent()) {
            return "Error: item " + id + " not found!";
        }

        if (mediaType.equalsIgnoreCase(String.valueOf(MediaType.ARTICLE))) {
            Article a = optItem.map(item -> ((Article) item)).orElse(new Article("", "", ""));
            model.put("author", a.getAuthor());
            model.put("source", a.getSource());
        }
        if (mediaType.equalsIgnoreCase(String.valueOf(MediaType.BOOK))) {
            Book b = optItem.map(item -> ((Book) item)).orElse(new Book("", "", ""));
            model.put("author", b.getAuthor());
            model.put("publisher", b.getPublisher());
        }
        if (mediaType.equalsIgnoreCase(String.valueOf(MediaType.CD))) {
            CD cd = optItem.map(item -> ((CD) item)).orElse(new CD("", "", ""));
            model.put("singer", cd.getSinger());
            model.put("album", cd.getAlbum());
        }
        if (mediaType.equalsIgnoreCase(String.valueOf(MediaType.DVD))) {
            DVD dvd = optItem.map(item -> ((DVD) item)).orElse(new DVD("", 0, false));
            model.put("duration", String.valueOf(dvd.getDuration()));
            model.put("fullHD", String.valueOf(dvd.isFullHD()));
        }
        if (mediaType.equalsIgnoreCase(String.valueOf(MediaType.MAGAZINE))) {
            Magazine m = optItem.map(item -> ((Magazine) item)).orElse(new Magazine("", null, ""));
            model.put("dateOfPublication", String.valueOf(m.getDateOfPublication()));
            model.put("publisher", m.getPublisher());
        }
        return SparkUtil.render(model, "details.vm");
    }

    public static Object handleItemDeleteRequest(Request req, Response res) {
        String id = req.params("id");
        try {
            libraryDao.delete(Integer.valueOf(id));
        } catch (Exception e) {
            System.out.println("Error deleting item with id '" + id + "': " + e.getMessage());
        }
        res.redirect("/main");
        return res;
    }

}



