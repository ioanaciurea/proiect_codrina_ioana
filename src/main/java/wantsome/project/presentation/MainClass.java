package wantsome.project.presentation;

import org.mindrot.jbcrypt.BCrypt;
import wantsome.project.database.dao.LibraryDao;
import wantsome.project.database.dao.UserDao;
import wantsome.project.database.ddl.LibraryCreator;
import wantsome.project.database.ddl.UsersCreator;
import wantsome.project.database.dto.library.*;
import wantsome.project.database.dto.users.User;
import wantsome.project.database.dto.users.UserType;
import wantsome.project.presentation.controller.library_controller.AddUpdateItem;
import wantsome.project.presentation.controller.library_controller.LibraryController;
import wantsome.project.presentation.controller.user_controller.AdminController;
import wantsome.project.presentation.controller.user_controller.LoginController;

import java.sql.Date;
import java.util.Arrays;

import static spark.Spark.*;

public class MainClass {

    public static void main(String[] args) {

        UsersCreator.createUsersTable();
        LibraryCreator.createMediaEntityTable();
        setInitialAdminUser();
        populateEmptyMediaEntityTable();

        staticFiles.location("/public");
        configureRoutes();

        System.out.println("Server started: http://localhost:4567/login");
    }

    private static void configureRoutes() {
        get("/login", (request, response) -> LoginController.mainPageLogin("", "", ""));
        post("/login", (request, response) -> LoginController.verifyCredentials(request, response));
        get("/logout", (request, response) -> LoginController.handleLogoutPost(request, response));


        get("/mainAdmin", (request, response) -> AdminController.adminMenuPage(request, response, ""));
        post("/mainAdmin", (request, response) -> AdminController.adminMenuPage(request, response, ""));
        get("/deleteAdmin/:id", (request, response) -> AdminController.handleUserDeleteRequest(request, response));
        get("/addUser", (request, response) -> AdminController.showAddUpdateUserForm(request, response));
        post("/addUser", (request, response) -> AdminController.handleAddUpdateUserRequest(request, response));
        get("/updateUser/:id", (request, response) -> AdminController.showAddUpdateUserForm(request, response));
        post("/updateUser/:id", (request, response) -> AdminController.handleAddUpdateUserRequest(request, response));


        get("/main", (request, response) -> LibraryController.libraryMenuPage(request, response));
        post("/main", (request, response) -> LibraryController.libraryMenuPage(request, response));
        get("/addItem", (request, response) -> AddUpdateItem.showAddUpdateItemForm(request, response));
        post("/addItem", (request, response) -> AddUpdateItem.handleAddUpdateItemRequest(request, response));
        get("/details/:id", ((request, response) -> LibraryController.handleItemDetails(request, response)));
        post("/details/:id", ((request, response) -> LibraryController.handleItemDetails(request, response)));
        get("/updateItem/:id", (request, response) -> AddUpdateItem.showAddUpdateItemForm(request, response));
        post("/updateItem/:id", (request, response) -> AddUpdateItem.handleAddUpdateItemRequest(request, response));
        get("/delete/:id", (request, response) -> LibraryController.handleItemDeleteRequest(request, response));

    }

    private static void setInitialAdminUser() {
        UserDao userDao = new UserDao();
        String hashedSalt = BCrypt.gensalt();
        if (userDao.getByType(UserType.ADMINISTRATOR).size() == 0) {
            userDao.insert(new User("admin", hashedSalt, BCrypt.hashpw("1234", hashedSalt), "Marian Popescu", "marianpopescu@gmail.com", UserType.ADMINISTRATOR));
            userDao.insert(new User("librarian", hashedSalt, BCrypt.hashpw("0000", hashedSalt), "Bianca Antonescu", "librarian@gmail.com", UserType.LIBRARIAN));
        }
    }

    private static void populateEmptyMediaEntityTable() {
        LibraryDao libraryDao = new LibraryDao();
        if (libraryDao.getAll().size() == 0) {
            Article a1 = new Article("Active metabolites of some exotic fruits", "Hannah M.", "ScienceDirect");
            Book b1 = new Book("In cautarea fericirii", "Bertrand R.", "Humanitas");
            Book b2 = new Book("Viitorul mintii umane", "Michio Kaku", "Trei");
            DVD dvd = new DVD("The thoughts that once we had", 160, true);
            CD cd = new CD("abc", "Mozart", "ABC");
            Magazine m = new Magazine("The New York Times", Date.valueOf("2020-01-05"), "The New York Times");
            libraryDao.insertAll(Arrays.asList(a1, b1, b2, dvd, cd, m));
        }
    }
}
