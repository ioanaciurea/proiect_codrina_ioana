package wantsome.project.database.dto.library;

public enum ItemState {
    AVAILABLE, BORROWED, LOST
}
