package wantsome.project.database.dto.library;

import java.util.Objects;

public class CD extends MediaEntity {
    private final String singer;
    private final String album;

    public CD(String title, String singer, String album) {
        super(MediaType.CD, title);
        this.singer = singer;
        this.album = album;
    }

    public String getSinger() {
        return singer;
    }

    public String getAlbum() {
        return album;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CD)) return false;
        if (!super.equals(o)) return false;
        CD cd = (CD) o;
        return Objects.equals(getSinger(), cd.getSinger()) &&
                Objects.equals(getAlbum(), cd.getAlbum());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSinger(), getAlbum());
    }

    @Override
    public String toString() {
        return "CD{" +
                "singer='" + singer + '\'' +
                ", album='" + album + '\'' +
                "} " + super.toString();
    }
}
