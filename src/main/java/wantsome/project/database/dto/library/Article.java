package wantsome.project.database.dto.library;

import java.util.Objects;

public class Article extends MediaEntity {
    private final String author;
    private final String source;

    public Article(String title, String author, String source) {
        super(MediaType.ARTICLE, title);
        this.author = author;
        this.source = source;
    }

    public String getAuthor() {
        return author;
    }

    public String getSource() {
        return source;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Article)) return false;
        if (!super.equals(o)) return false;
        Article article = (Article) o;
        return Objects.equals(getAuthor(), article.getAuthor()) &&
                Objects.equals(getSource(), article.getSource());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAuthor(), getSource());
    }

    @Override
    public String toString() {
        return "Article{" +
                "author='" + author + '\'' +
                ", source='" + source + '\'' +
                "} " + super.toString();
    }
}
