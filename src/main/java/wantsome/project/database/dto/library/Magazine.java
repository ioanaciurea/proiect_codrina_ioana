package wantsome.project.database.dto.library;

import java.sql.Date;
import java.util.Objects;

public class Magazine extends MediaEntity {
    private final Date dateOfPublication;
    private final String publisher;

    public Magazine(String title, java.sql.Date dateOfPublication, String publisher) {
        super(MediaType.MAGAZINE, title);
        this.dateOfPublication = dateOfPublication;
        this.publisher = publisher;
    }

    public java.sql.Date getDateOfPublication() {
        return dateOfPublication;
    }

    public String getPublisher() {
        return publisher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Magazine)) return false;
        if (!super.equals(o)) return false;
        Magazine magazine = (Magazine) o;
        return Objects.equals(getDateOfPublication(), magazine.getDateOfPublication()) &&
                Objects.equals(getPublisher(), magazine.getPublisher());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getDateOfPublication(), getPublisher());
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "dateOfPublication=" + dateOfPublication +
                ", publisher='" + publisher + '\'' +
                "} " + super.toString();
    }
}
