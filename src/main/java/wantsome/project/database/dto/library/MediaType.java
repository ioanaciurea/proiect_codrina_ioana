package wantsome.project.database.dto.library;

public enum MediaType {
    ARTICLE, BOOK, CD, DVD, MAGAZINE
}
