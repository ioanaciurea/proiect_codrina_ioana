package wantsome.project.database.dto.library;

import java.sql.Date;
import java.util.Objects;

public class MediaEntity {
    private int mediaId;
    private final MediaType type;
    private final String title;
    private ItemState state;
    private int studentId;
    private java.sql.Date startDate;
    private java.sql.Date dueDate;
    private Date returnDate;

    //Make the constructor protected (not public) to prevent construction of MediaEntity instances directly (only children are ok to be built)
    protected MediaEntity(MediaType type, String title) {
        this.type = type;
        this.title = title;
        this.state = ItemState.AVAILABLE;
        /*this.startDate = Date.valueOf("0000-00-00");
        this.dueDate = Date.valueOf("0000-00-00");
        this.returnDate = Date.valueOf("0000-00-00");*/
    }

    public int getMediaId() {
        return mediaId;
    }

    public MediaType getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public ItemState getState() {
        return state;
    }

    public int getStudentId() {
        return studentId;
    }

    public java.sql.Date getStartDate() {
        return startDate;
    }

    public java.sql.Date getDueDate() {
        return dueDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setMediaId(int mediaId) {
        this.mediaId = mediaId;
    }

    public void setState(ItemState state) {
        this.state = state;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public void setProperties(int mediaId, ItemState itemState, int studentId, Date startDate, Date dueDate, Date returnDate) {
        setMediaId(mediaId);
        setState(itemState);
        setStudentId(studentId);
        setStartDate(startDate);
        setDueDate(dueDate);
        setReturnDate(returnDate);
    }

    public void setHistoryProperties(Date startDate, Date dueDate, Date returnDate, int studentId) {
        setStartDate(startDate);
        setDueDate(dueDate);
        setReturnDate(returnDate);
        setStudentId(studentId);
    }

    @Override
    public String toString() {
        return "MediaEntity{" +
                "mediaId=" + mediaId +
                ", type=" + type +
                ", title='" + title + '\'' +
                ", state=" + state +
                ", studentId=" + studentId +
                ", startDate=" + startDate +
                ", dueDate=" + dueDate +
                ", returnDate=" + returnDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MediaEntity)) return false;
        MediaEntity that = (MediaEntity) o;
        return getMediaId() == that.getMediaId() &&
                getStudentId() == that.getStudentId() &&
                getType() == that.getType() &&
                Objects.equals(getTitle(), that.getTitle()) &&
                getState() == that.getState() &&
                Objects.equals(getStartDate(), that.getStartDate()) &&
                Objects.equals(getDueDate(), that.getDueDate()) &&
                Objects.equals(getReturnDate(), that.getReturnDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMediaId(), getType(), getTitle(), getState(), getStudentId(), getStartDate(), getDueDate(), getReturnDate());
    }
}
