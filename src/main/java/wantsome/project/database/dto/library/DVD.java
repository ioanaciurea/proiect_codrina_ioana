package wantsome.project.database.dto.library;

import java.util.Objects;

public class DVD extends MediaEntity {
    private final int duration;
    private final boolean fullHD;

    public DVD(String title, int duration, boolean fullHD) {
        super(MediaType.DVD, title);
        this.duration = duration;
        this.fullHD = fullHD;
    }

    public int getDuration() {
        return duration;
    }

    public boolean isFullHD() {
        return fullHD;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DVD)) return false;
        if (!super.equals(o)) return false;
        DVD dvd = (DVD) o;
        return getDuration() == dvd.getDuration() &&
                isFullHD() == dvd.isFullHD();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getDuration(), isFullHD());
    }

    @Override
    public String toString() {
        return "DVD{" +
                "duration=" + duration +
                ", fullHD=" + fullHD +
                "} " + super.toString();
    }
}
