package wantsome.project.database.dto.users;

import java.util.Objects;

public class User {

    private int id;
    private String loginName;
    private String password;
    private String realName;
    private String email;
    private UserType type;
    private String hashSalt;

    public User(String loginName, String hashSalt, String password, String realName, String email, UserType type) {
        this.loginName = loginName;
        this.hashSalt = hashSalt;
        this.password = password;
        this.realName = realName;
        this.email = email;
        this.type = type;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public String getPassword() {
        return password;
    }

    public String getRealName() {
        return realName;
    }

    public String getEmail() {
        return email;
    }


    public String getHashSalt() {
        return hashSalt;
    }

    public UserType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getId() == user.getId() &&
                getLoginName().equals(user.getLoginName()) &&
                getPassword().equals(user.getPassword()) &&
                getRealName().equals(user.getRealName()) &&
                getEmail().equals(user.getEmail()) &&
                getType() == user.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getLoginName(), getPassword(), getRealName(), getEmail(), getType());
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", loginName='" + loginName + '\'' +
                ", password='" + password + '\'' +
                ", realName='" + realName + '\'' +
                ", email='" + email + '\'' +
                ", type=" + type +
                '}';
    }
}
