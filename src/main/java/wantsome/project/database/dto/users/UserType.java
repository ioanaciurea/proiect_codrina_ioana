package wantsome.project.database.dto.users;

public enum UserType {
    ADMINISTRATOR, LIBRARIAN
}
