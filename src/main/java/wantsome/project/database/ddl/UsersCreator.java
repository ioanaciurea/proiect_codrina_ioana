package wantsome.project.database.ddl;

import wantsome.project.database.DatabaseManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import static wantsome.project.database.dto.users.UserType.ADMINISTRATOR;
import static wantsome.project.database.dto.users.UserType.LIBRARIAN;

public class UsersCreator {

    public static void createUsersTable() {

        String query = "CREATE TABLE IF NOT EXISTS USERS(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "LOGIN_NAME TEXT UNIQUE NOT NULL," +
                "HASHSALT TEXT  NOT NULL," +
                "PASSWORD TEXT NOT NULL," +
                "REAL_NAME TEXT NOT NULL," +
                "EMAIL TEXT NOT NULL," +
                "TYPE TEXT CHECK (TYPE IN ('" + ADMINISTRATOR + "','" + LIBRARIAN + "')) NOT NULL" + ")";

        try (Connection conn = DatabaseManager.getConnection();
             Statement st = conn.createStatement()) {

            st.execute(query);
        } catch (SQLException e) {
            System.err.println("Error creating Users table: " + e.getMessage());
        }
    }

    public static void dropUsersTable() {
        String query = "DROP TABLE USERS";
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(query)) {
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error while trying to drop table Users: " + e.getMessage());
        }
    }
}
