package wantsome.project.database.ddl;

import wantsome.project.database.DatabaseManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static wantsome.project.database.dto.library.ItemState.*;
import static wantsome.project.database.dto.library.MediaType.*;

public class LibraryCreator {
    public static void createMediaEntityTable() {
        try (Connection conn = DatabaseManager.getConnection();
             Statement statement = conn.createStatement()) {
            String query = "CREATE TABLE IF NOT EXISTS MEDIA_ENTITY ( " +
                    "MEDIA_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "MEDIA_TYPE TEXT CHECK (MEDIA_TYPE IN ('" + BOOK + "','" + ARTICLE + "','" + CD + "','" + DVD + "','" + MAGAZINE + "')) NOT NULL, " +
                    "TITLE TEXT UNIQUE NOT NULL, " +
                    "STATE TEXT CHECK (STATE IN ('" + AVAILABLE + "','" + BORROWED + "','" + LOST + "')) NOT NULL, " +
                    "STUDENT_ID INTEGER, " +
                    "START_DATE DATETIME, " +
                    "DUE_DATE DATETIME, " +
                    "RETURN_DATE DATETIME, " +
                    "AUTHOR TEXT, " +
                    "PUBLISHER TEXT, " +
                    "ARTICLE_SOURCE TEXT, " +
                    "SINGER TEXT, " +
                    "ALBUM TEXT, " +
                    "DURATION INTEGER, " +
                    "FULL_HD TEXT CHECK (FULL_HD IN ('" + TRUE + "','" + FALSE + "')), " +
                    "DATE_OF_PUBLICATION DATETIME ) ";
            statement.execute(query);
        } catch (SQLException e) {
            System.err.println("Error creating MediaEntity table: " + e.getMessage());
        }
    }

    public static void createStudentsTable() {
        try (Connection conn = DatabaseManager.getConnection();
             Statement statement = conn.createStatement()) {
            String query = "CREATE TABLE IF NOT EXISTS STUDENTS ( " +
                    "STUDENT_ID INTEGER PRIMARY KEY CHECK (LENGTH(STUDENT_ID) <= 7 AND STUDENT_ID > 0), " +
                    "STUDENT_NAME TEXT NOT NULL," +
                    "EMAIL TEXT ) ";
            statement.execute(query);
        } catch (SQLException e) {
            System.err.println("Error creating STUDENTS table: " + e.getMessage());
        }
    }

    public static void createItemHistoryTable() {
        try (Connection conn = DatabaseManager.getConnection();
             Statement statement = conn.createStatement()) {
            String query = "CREATE TABLE IF NOT EXISTS ITEM_HISTORY ( " +
                    "MEDIA_ENTITY_ID INTEGER REFERENCES MEDIA_ENTITY (MEDIA_ID), " +
                    "STUDENT_NAME TEXT NOT NULL REFERENCES STUDENTS (STUDENT_NAME), " +
                    "START_DATE DATETIME, " +
                    "DUE_DATE DATETIME, " +
                    "RETURN_DATE DATETIME, " +
                    "ITEM_STATE TEXT ) ";
            statement.execute(query);
        } catch (SQLException e) {
            System.err.println("Error creating ITEM HISTORY table: " + e.getMessage());
        }
    }

    public static void dropTable_mediaEntity() {
        String DROP_TABLE = "DROP TABLE MEDIA_ENTITY";
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(DROP_TABLE)) {
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error while trying to drop table MEDIA_ENTITY: " + e.getMessage());
        }
    }

    public static void dropTable_students() {
        String DROP_TABLE = "DROP TABLE STUDENTS";
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(DROP_TABLE)) {
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error while trying to drop table STUDENTS: " + e.getMessage());
        }
    }

    public static void dropTable_itemHistory() {
        String DROP_TABLE = "DROP TABLE ITEM_HISTORY";
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(DROP_TABLE)) {
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error while trying to drop table ITEM_HISTORY: " + e.getMessage());
        }
    }
}
