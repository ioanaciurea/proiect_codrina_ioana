package wantsome.project.database.dao;

import wantsome.project.database.DatabaseManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

abstract class AbstractDao<T> {
    /**
     * Load list of objects from DB (all found)
     */
    public List<T> getAll() {
        try (Connection connection = DatabaseManager.getConnection()) {
            return executeSelect(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * Load a specific object from DB (by id)
     */
    public Optional<T> get(int id) {
        try (Connection connection = DatabaseManager.getConnection()) {
            return Optional.ofNullable(executeSelect(connection, id));
        } catch (SQLException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    /**
     * Add a new object to DB
     */
    public void insert(T object) {
        try (Connection connection = DatabaseManager.getConnection()) {
            executeInsert(connection, object);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertAll(List<T> objects) {
        try (Connection connection = DatabaseManager.getConnection()) {
            for (T obj : objects) {
                executeInsert(connection, obj);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update an existing object in DB
     */
    public void update(T object) {
        try (Connection connection = DatabaseManager.getConnection()) {
            executeUpdate(connection, object);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete an object from DB
     */
    public void delete(int id) {
        try (Connection connection = DatabaseManager.getConnection()) {
            executeDelete(connection, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        try (Connection connection = DatabaseManager.getConnection()) {
            executeDeleteAll(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*--- To be implemented in specific way by each specific DTO! ---*/

    protected abstract List<T> executeSelect(Connection connection);

    protected abstract T executeSelect(Connection connection, int id);

    protected abstract void executeInsert(Connection connection, T object);

    protected abstract void executeUpdate(Connection connection, T object);

    protected abstract void executeDelete(Connection connection, int id);

    protected abstract void executeDeleteAll(Connection connection);
}
