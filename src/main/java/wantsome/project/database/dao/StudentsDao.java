package wantsome.project.database.dao;

import wantsome.project.database.DatabaseManager;
import wantsome.project.database.dto.library.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StudentsDao {
    protected void insert(Student student) {
        final String INSERT = "INSERT INTO STUDENTS (STUDENT_ID, STUDENT_NAME, EMAIL) VALUES (?, ?, ?)";
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(INSERT)) {
            ps.setInt(1, student.getStudentId());
            ps.setString(2, student.getStudentName());
            ps.setString(3, student.getEmail());

            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected List<Student> selectAll() {
        final String SELECT_ALL = "SELECT * FROM STUDENTS ORDER BY STUDENT_ID";
        List<Student> list = new ArrayList<>();
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(SELECT_ALL)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    int studentId = rs.getInt("STUDENT_ID");
                    String studentName = rs.getString("STUDENT_NAME");
                    String email = rs.getString("EMAIL");
                    list.add(new Student(studentId, studentName, email));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    protected List<Student> select(String name) {
        final String SELECT_BY_NAME = "SELECT * FROM STUDENTS WHERE STUDENT_NAME LIKE ?";
        List<Student> list = new ArrayList<>();
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(SELECT_BY_NAME)) {
            ps.setString(1, "%" + name + "%");
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    int studentId = rs.getInt("STUDENT_ID");
                    String studentName = rs.getString("STUDENT_NAME");
                    String email = rs.getString("EMAIL");
                    list.add(new Student(studentId, studentName, email));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    protected Optional<Student> selectById(String searchedId) {
        final String SELECT_BY_CNP = "SELECT * FROM STUDENTS WHERE STUDENT_ID = ?";
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(SELECT_BY_CNP)) {
            ps.setString(1, searchedId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    int studentId = rs.getInt("STUDENT_ID");
                    String studentName = rs.getString("STUDENT_NAME");
                    String email = rs.getString("EMAIL");
                    return Optional.of(new Student(studentId, studentName, email));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
