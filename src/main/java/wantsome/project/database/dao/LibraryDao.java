package wantsome.project.database.dao;

import wantsome.project.database.DatabaseManager;
import wantsome.project.database.dto.library.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LibraryDao extends AbstractDao<MediaEntity> {

    @Override
    protected List<MediaEntity> executeSelect(Connection connection) {
        List<MediaEntity> results = new ArrayList<>();
        final String READ_ALL_QUERY = "SELECT * FROM MEDIA_ENTITY ORDER BY MEDIA_ID";

        try (PreparedStatement ps = connection.prepareStatement(READ_ALL_QUERY);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                MediaEntity item = extractItemFromResult(rs);
                results.add(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return results;
    }

    private MediaEntity extractItemFromResult(ResultSet resultSet) throws SQLException {

        int mediaId = resultSet.getInt("MEDIA_ID");
        MediaType mediaType = MediaType.valueOf(resultSet.getString("MEDIA_TYPE"));
        String title = resultSet.getString("TITLE");
        ItemState itemState = ItemState.valueOf(resultSet.getString("STATE"));
        int studentId = resultSet.getInt("STUDENT_ID");

        Date startDate = null;

        Date dbStartDate = resultSet.getDate("START_DATE");
        if (dbStartDate != null) {
            startDate = new Date(dbStartDate.getTime());
        }
        Date dueDate = null;
        Date dbDueDate = resultSet.getDate("DUE_DATE");
        if (dbDueDate != null) {
            dueDate = new Date(dbDueDate.getTime());
        }

        Date returnDate = null;
        Date dbReturnDate = resultSet.getDate("RETURN_DATE");
        if (dbReturnDate != null) {
            returnDate = new Date(dbReturnDate.getTime());
        }


        switch (mediaType) {
            case ARTICLE:
                Article a = new Article(title, resultSet.getString("AUTHOR"), resultSet.getString("ARTICLE_SOURCE"));
                a.setProperties(mediaId, itemState, studentId, startDate, dueDate, returnDate);
                return a;
            case BOOK:
                Book b = new Book(title, resultSet.getString("AUTHOR"), resultSet.getString("PUBLISHER"));
                b.setProperties(mediaId, itemState, studentId, startDate, dueDate, returnDate);
                return b;
            case CD:
                CD cd = new CD(title, resultSet.getString("SINGER"), resultSet.getString("ALBUM"));
                cd.setProperties(mediaId, itemState, studentId, startDate, dueDate, returnDate);
                return cd;
            case DVD:
                DVD dvd = new DVD(title, resultSet.getInt("DURATION"), Boolean.parseBoolean(resultSet.getString("FULL_HD")));
                //resultSet.getBoolean("FULL_HD") -> nu functioneaza deoarece in DB FULL_HD este de tip text,
                // deci .getBoolean nu gaseste niciun Boolean pe coloana FULL_HD si intoarce mereu FALSE

                dvd.setProperties(mediaId, itemState, studentId, startDate, dueDate, returnDate);
                return dvd;
            case MAGAZINE:
                Magazine m = new Magazine(title, resultSet.getDate("DATE_OF_PUBLICATION"), resultSet.getString("PUBLISHER"));
                m.setProperties(mediaId, itemState, studentId, startDate, dueDate, returnDate);
                return m;
        }
        return new Article(title, resultSet.getString("AUTHOR"), resultSet.getString("ARTICLE_SOURCE"));
    }


    @Override
    protected MediaEntity executeSelect(Connection connection, int mediaId) {
        final String READ_QUERY = "SELECT * FROM MEDIA_ENTITY WHERE MEDIA_ID = ?";
        try (PreparedStatement ps = connection.prepareStatement(READ_QUERY)) {
            ps.setInt(1, mediaId);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return extractItemFromResult(rs);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void executeInsert(Connection connection, MediaEntity mediaEntity) {
        final String INSERT_QUERY = "INSERT INTO MEDIA_ENTITY (MEDIA_TYPE, TITLE, STATE, STUDENT_ID, START_DATE, DUE_DATE, RETURN_DATE, AUTHOR, PUBLISHER, ARTICLE_SOURCE, SINGER, ALBUM, DURATION, FULL_HD, DATE_OF_PUBLICATION) values (?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(INSERT_QUERY)) {
            ps.setString(1, mediaEntity.getType().name());
            ps.setString(2, mediaEntity.getTitle());
            ps.setString(3, mediaEntity.getState().name());
            ps.setInt(4, mediaEntity.getStudentId());
            ps.setDate(5, mediaEntity.getStartDate());
            ps.setDate(6, mediaEntity.getDueDate());
            ps.setDate(7, mediaEntity.getReturnDate());

            switchCaseInsert(mediaEntity, ps);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void switchCaseInsert(MediaEntity mediaEntity, PreparedStatement ps) throws SQLException {
        switch (mediaEntity.getType()) {
            case ARTICLE:
                Article a = (Article) mediaEntity;
                ps.setString(8, a.getAuthor());
                ps.setString(9, null);
                ps.setString(10, a.getSource());
                ps.setString(11, null);
                ps.setString(12, null);
                ps.setInt(13, -1);
                ps.setString(14, String.valueOf(false));
                ps.setDate(15, null);
                break;
            case BOOK:
                Book b = (Book) mediaEntity;
                ps.setString(8, b.getAuthor());
                ps.setString(9, b.getPublisher());
                ps.setString(10, null);
                ps.setString(11, null);
                ps.setString(12, null);
                ps.setInt(13, -1);
                ps.setString(14, String.valueOf(false));
                ps.setDate(15, null);
                break;
            case CD:
                CD cd = (CD) mediaEntity;
                ps.setString(8, null);
                ps.setString(9, null);
                ps.setString(10, null);
                ps.setString(11, cd.getSinger());
                ps.setString(12, cd.getAlbum());
                ps.setInt(13, -1);
                ps.setString(14, String.valueOf(false));
                ps.setDate(15, null);
                break;
            case DVD:
                DVD dvd = (DVD) mediaEntity;
                ps.setString(8, null);
                ps.setString(9, null);
                ps.setString(10, null);
                ps.setString(11, null);
                ps.setString(12, null);
                ps.setInt(13, dvd.getDuration());
                ps.setString(14, String.valueOf(dvd.isFullHD()));
                ps.setDate(15, null);
                break;
            case MAGAZINE:
                Magazine m = (Magazine) mediaEntity;
                ps.setString(8, null);
                ps.setString(9, m.getPublisher());
                ps.setString(10, null);
                ps.setString(11, null);
                ps.setString(12, null);
                ps.setInt(13, -1);
                ps.setString(14, String.valueOf(false));
                ps.setDate(15, m.getDateOfPublication());

        }
    }

    @Override
    protected void executeUpdate(Connection connection, MediaEntity mediaEntity) {
        final String UPDATE_QUERY = "UPDATE MEDIA_ENTITY SET AUTHOR = ?, PUBLISHER = ?, ARTICLE_SOURCE = ?, SINGER = ?, ALBUM = ?, DURATION = ?, FULL_HD = ?, DATE_OF_PUBLICATION = ?  WHERE TITLE = ?";
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_QUERY)) {

            switch (mediaEntity.getType()) {
                case ARTICLE:
                    Article a = (Article) mediaEntity;
                    ps.setString(1, a.getAuthor());
                    ps.setString(2, null);
                    ps.setString(3, a.getSource());
                    ps.setString(4, null);
                    ps.setString(5, null);
                    ps.setInt(6, -1);
                    ps.setString(7, String.valueOf(false));
                    ps.setDate(8, null);
                    break;
                case BOOK:
                    Book b = (Book) mediaEntity;
                    ps.setString(1, b.getAuthor());
                    ps.setString(2, b.getPublisher());
                    ps.setString(3, null);
                    ps.setString(4, null);
                    ps.setString(5, null);
                    ps.setInt(6, -1);
                    ps.setString(7, String.valueOf(false));
                    ps.setDate(8, null);
                    break;
                case CD:
                    CD cd = (CD) mediaEntity;
                    ps.setString(1, null);
                    ps.setString(2, null);
                    ps.setString(3, null);
                    ps.setString(4, cd.getSinger());
                    ps.setString(5, cd.getAlbum());
                    ps.setInt(6, -1);
                    ps.setString(7, String.valueOf(false));
                    ps.setDate(8, null);
                    break;
                case DVD:
                    DVD dvd = (DVD) mediaEntity;
                    ps.setString(1, null);
                    ps.setString(2, null);
                    ps.setString(3, null);
                    ps.setString(4, null);
                    ps.setString(5, null);
                    ps.setInt(6, dvd.getDuration());
                    ps.setString(7, String.valueOf(dvd.isFullHD()));
                    ps.setDate(8, null);
                    break;
                case MAGAZINE:
                    Magazine m = (Magazine) mediaEntity;
                    ps.setString(1, null);
                    ps.setString(2, m.getPublisher());
                    ps.setString(3, null);
                    ps.setString(4, null);
                    ps.setString(5, null);
                    ps.setInt(6, -1);
                    ps.setString(7, String.valueOf(false));
                    ps.setDate(8, m.getDateOfPublication());

            }
            ps.setString(9, mediaEntity.getTitle());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error while trying to update: " + mediaEntity + " : " + e.getMessage());
        }
    }

    public static void updateState(MediaEntity mediaEntity) {
        final String UPDATE_QUERY = "UPDATE MEDIA_ENTITY SET STATE = ?, STUDENT_ID = ?, START_DATE = ?, DUE_DATE = ?, RETURN_DATE = ? WHERE TITLE = ?";
        final String FIND_ITEM_WITH_SAME_TITLE = "SELECT MEDIA_ID FROM MEDIA_ENTITY WHERE TITLE = ?";
        Optional<MediaEntity> currentItem = Optional.empty();
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_ITEM_WITH_SAME_TITLE)) {
            ps.setString(1, mediaEntity.getTitle());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                currentItem = new LibraryDao().get(rs.getInt("MEDIA_ID"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (currentItem.isPresent() && !mediaEntity.getState().equals(currentItem.get().getState())) {
            if (!currentItem.get().getState().equals(ItemState.LOST)) { //items declared LOST will not be updated
                if (currentItem.get().getState().equals(ItemState.AVAILABLE)) {
                    //that means the update will be on BORROWED or LOST
                    try (Connection connection = DatabaseManager.getConnection();
                         PreparedStatement ps = connection.prepareStatement(UPDATE_QUERY)) {
                        ps.setString(1, String.valueOf(mediaEntity.getState()));
                        ps.setInt(2, mediaEntity.getStudentId());
                        ps.setDate(3, mediaEntity.getStartDate());
                        ps.setDate(4, mediaEntity.getDueDate());
                        ps.setDate(5, null);
                        ps.setString(6, mediaEntity.getTitle());
                        ps.executeUpdate();
                    } catch (SQLException e) {
                        System.out.println("Error while trying to update " + currentItem + ": " + e.getMessage());
                    }
                }
                if (currentItem.get().getState().equals(ItemState.BORROWED)) {
                    //that means the update will be on AVAILABLE or LOST
                    if (ItemState.AVAILABLE.equals(mediaEntity.getState())) {
                        try (Connection connection = DatabaseManager.getConnection();
                             PreparedStatement ps = connection.prepareStatement(UPDATE_QUERY)) {
                            ps.setString(1, String.valueOf(ItemState.AVAILABLE));
                            ps.setInt(2, 0);
                            ps.setDate(3, null);
                            ps.setDate(4, null);
                            ps.setDate(5, null);
                            ps.setString(6, mediaEntity.getTitle());
                            ps.executeUpdate();
                        } catch (SQLException e) {
                            System.out.println("Error while trying to update " + currentItem + ": " + e.getMessage());
                        }
                    }
                    if (ItemState.LOST.equals(mediaEntity.getState())) {
                        try (Connection connection = DatabaseManager.getConnection();
                             PreparedStatement ps = connection.prepareStatement(UPDATE_QUERY)) {
                            ps.setString(1, String.valueOf(ItemState.LOST));
                            ps.setInt(2, mediaEntity.getStudentId());
                            ps.setDate(3, mediaEntity.getStartDate());
                            ps.setDate(4, mediaEntity.getDueDate());
                            ps.setDate(5, null);
                            ps.setString(6, mediaEntity.getTitle());
                            ps.executeUpdate();
                        } catch (SQLException e) {
                            System.out.println("Error while trying to update " + currentItem + ": " + e.getMessage());
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void executeDelete(Connection connection, int id) {
        final String DELETE_QUERY = "DELETE FROM MEDIA_ENTITY WHERE MEDIA_ID = ?";
        try (PreparedStatement ps = connection.prepareStatement(DELETE_QUERY)) {
            ps.setInt(1, id);

            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error while trying to delete item " + id + " : " + e.getMessage());
        }
    }

    @Override
    protected void executeDeleteAll(Connection connection) {
        final String DELETE_ALL_QUERY = "DELETE FROM MEDIA_ENTITY";
        try (PreparedStatement ps = connection.prepareStatement(DELETE_ALL_QUERY)) {
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error while trying to delete all items: " + e.getMessage());
        }
    }

    public Optional<MediaEntity> selectByTitle(String title) {
        final String SELECT_BY_TITLE = "SELECT * FROM MEDIA_ENTITY WHERE TITLE = ?";
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(SELECT_BY_TITLE)) {
            ps.setString(1, title);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    MediaEntity item = extractItemFromResult(rs);
                    return Optional.of(item);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public List<MediaEntity> selectByTitleContains(String title) {
        final String SELECT_BY_TITLE = "SELECT * FROM MEDIA_ENTITY WHERE TITLE LIKE ?";
        List<MediaEntity> list = new ArrayList<>();
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(SELECT_BY_TITLE)) {
            ps.setString(1, "%" + title + "%");
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    MediaEntity item = extractItemFromResult(rs);
                    list.add(item);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<MediaEntity> selectByState(ItemState itemState) {
        List<MediaEntity> results = new ArrayList<>();
        final String SELECT_BY_STATE = "SELECT * FROM MEDIA_ENTITY WHERE STATE = ?";
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(SELECT_BY_STATE)) {
            ps.setString(1, itemState.name());
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    MediaEntity item = extractItemFromResult(rs);
                    results.add(item);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return results;
    }


}
