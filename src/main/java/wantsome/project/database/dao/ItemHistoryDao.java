package wantsome.project.database.dao;

import wantsome.project.database.DatabaseManager;
import wantsome.project.database.dto.library.ItemState;
import wantsome.project.database.dto.library.MediaEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ItemHistoryDao {
    protected void insert(MediaEntity mediaEntity) {
        final String INSERT = "INSERT INTO ITEM_HISTORY (MEDIA_ENTITY_ID, STUDENT_NAME, START_DATE, DUE_DATE, RETURN_DATE, ITEM_STATE) VALUES (?, ?, ?, ?, ?, ?)";
        String studentName = findStudentName(mediaEntity.getStudentId());
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(INSERT)) {
            ps.setInt(1, mediaEntity.getMediaId());
            ps.setString(2, studentName);
            ps.setDate(3, mediaEntity.getStartDate());
            ps.setDate(4, mediaEntity.getDueDate());
            ps.setDate(5, mediaEntity.getReturnDate());
            ps.setString(6, mediaEntity.getState().name());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void insertAll(List<MediaEntity> list) {
        for (MediaEntity item : list) {
            insert(item);
        }
    }

    private String findStudentName(int studentId) {
        final String FIND_STUDENT = "SELECT STUDENT_NAME FROM STUDENTS WHERE STUDENT_ID = ?";
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_STUDENT)) {
            ps.setInt(1, studentId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getString("STUDENT_NAME");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "No student found!";
    }

    protected List<MediaEntity> selectFromHistoryByID(int mediaId) {
        final String SELECT_BY_ID = "SELECT * FROM ITEM_HISTORY WHERE MEDIA_ENTITY_ID = ?";
        List<MediaEntity> list = new ArrayList<>();
        final LibraryDao libraryDao = new LibraryDao();

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(SELECT_BY_ID)) {
            ps.setInt(1, mediaId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    MediaEntity item = libraryDao.executeSelect(connection, mediaId);
                    item.setStudentId(findStudentId(rs.getString("STUDENT_NAME")));
                    item.setStartDate(rs.getDate("START_DATE"));
                    item.setDueDate(rs.getDate("DUE_DATE"));
                    item.setReturnDate(rs.getDate("RETURN_DATE"));
                    item.setState(ItemState.valueOf(rs.getString("ITEM_STATE")));
                    list.add(item);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    private int findStudentId(String name) {
        final String FIND_STUDENT = "SELECT STUDENT_ID FROM STUDENTS WHERE STUDENT_NAME = ?";
        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_STUDENT)) {
            ps.setString(1, name);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt("STUDENT_ID");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
