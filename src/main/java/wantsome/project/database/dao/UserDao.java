package wantsome.project.database.dao;

import wantsome.project.database.DatabaseManager;
import wantsome.project.database.dto.users.User;
import wantsome.project.database.dto.users.UserType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class UserDao extends AbstractDao<User> {


    public Optional<User> getByLoginName(String login_name) {
        try (Connection connection = DatabaseManager.getConnection()) {
            return Optional.ofNullable(executeSelectByLoginName(connection, login_name));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<User> getByType(UserType type) {
        try (Connection connection = DatabaseManager.getConnection()) {
            return executeSelectbyType(connection, type);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }


    @Override
    protected List<User> executeSelect(Connection connection) {
        final String SELECT_ALL = "SELECT * FROM USERS ORDER BY ID";
        List<User> results = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(SELECT_ALL);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                results.add(extractUserFromResult(rs));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    protected User executeSelect(Connection connection, int id) {
        final String SELECT_BY_ID_QUERY = "SELECT * FROM USERS WHERE ID = ?";
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            ps.setInt(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return extractUserFromResult(rs);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected List<User> executeSelectbyType(Connection connection, UserType type) {
        final String SELECT_BY_ID_QUERY = "SELECT * FROM USERS WHERE TYPE = ?";
        List<User> results = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            ps.setString(1, type.name());

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    results.add(extractUserFromResult(rs));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    protected User executeSelectByLoginName(Connection connection, String login_name) {
        final String SELECT_BY_LOGIN_NAME_QUERY = "SELECT * FROM USERS WHERE LOGIN_NAME = ?";
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_LOGIN_NAME_QUERY)) {
            ps.setString(1, login_name);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return extractUserFromResult(rs);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void executeInsert(Connection connection, User user) {
        final String INSERT_QUERY = "INSERT INTO USERS (LOGIN_NAME, HASHSALT, PASSWORD, REAL_NAME, EMAIL, TYPE) values (?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(INSERT_QUERY)) {

            ps.setString(1, user.getLoginName());
            ps.setString(2, user.getHashSalt());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getRealName());
            ps.setString(5, user.getEmail());
            ps.setString(6, user.getType().name());

            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeUpdate(Connection connection, User user) {
        final String UPDATE_QUERY = "UPDATE USERS SET HASHSALT=?, PASSWORD = ?, REAL_NAME = ?, EMAIL = ? WHERE LOGIN_NAME = ?";
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_QUERY)) {
            ps.setString(1, user.getHashSalt());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getRealName());
            ps.setString(4, user.getEmail());
            ps.setString(5, user.getLoginName());

            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error while trying to update " + user + " : " + e.getMessage());
        }
    }

    @Override
    protected void executeDelete(Connection connection, int id) {
        final String DELETE_QUERY = "DELETE FROM USERS WHERE ID = ?";
        try (PreparedStatement ps = connection.prepareStatement(DELETE_QUERY)) {

            ps.setInt(1, id);

            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error while trying to delete user " + id + ": " + e.getMessage());
        }
    }

    @Override
    protected void executeDeleteAll(Connection connection) {
        final String DELETE_ALL = "DELETE FROM USERS";
        try (PreparedStatement ps = connection.prepareStatement(DELETE_ALL)) {
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error while trying to delete users: " + e.getMessage());
        }
    }

    private static User extractUserFromResult(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("ID");
        String login_name = resultSet.getString("LOGIN_NAME");
        String hashSalt = resultSet.getString("HASHSALT");
        String password = resultSet.getString("PASSWORD");
        String real_name = resultSet.getString("REAL_NAME");
        String email = resultSet.getString("EMAIL");
        UserType type = UserType.valueOf(resultSet.getString("TYPE"));
        User user = new User(login_name, hashSalt, password, real_name, email, type);
        user.setId(id);

        return user;
    }
}
