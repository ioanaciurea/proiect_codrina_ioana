package wantsome.project.database;

import org.sqlite.javax.SQLiteConnectionPoolDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseManager {

    private static final String JDBC_URL = "jdbc:sqlite:library_management.db";

    public static Connection getConnection() throws SQLException {
        return getPooledDataSource().getConnection();
    }

    /**
     /**
     * More efficient version, uses a connection pool (so it recycles the connections)
     */
    private static SQLiteConnectionPoolDataSource getPooledDataSource() {
        SQLiteConnectionPoolDataSource ds = new SQLiteConnectionPoolDataSource();
        ds.setUrl(JDBC_URL);
        ds.setEnforceForeignKeys(true); //enable FK support (disabled by default)
        return ds;
    }

}
