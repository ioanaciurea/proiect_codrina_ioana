package wantsome.project.database.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import wantsome.project.database.ddl.LibraryCreator;
import wantsome.project.database.dto.library.*;

import java.sql.Date;
import java.util.Arrays;
import java.util.Optional;

public class LibraryDaoTest {


    private Article a1 = new Article("Active metabolites of some exotic fruits", "Hannah M.", "ScienceDirect");
    private Book b1 = new Book("In cautarea fericirii", "Bertrand R.", "Humanitas");
    private Book b2 = new Book("Viitorul mintii umane", "Michio Kaku", "Trei");
    private DVD dvd = new DVD("The thoughts that once we had", 160, true);
    private CD cd = new CD("abc", "Mozart", "ABC");
    private Magazine m = new Magazine("The New York Times", Date.valueOf("2020-01-05"), "The New York Times");

    private Article aHistory = new Article(a1.getTitle(), a1.getAuthor(), a1.getSource());
    private Book bHistory = new Book(b1.getTitle(), b1.getAuthor(), b1.getPublisher());

    Student s1 = new Student(2, "Ion", "ion@gmail.com");
    Student s2 = new Student(4, "Marian", "marian@gmail.com");

    final private LibraryDao libraryDao = new LibraryDao();
    final private ItemHistoryDao itemHistoryDao = new ItemHistoryDao();
    final private StudentsDao studentsDao = new StudentsDao();

    @Before
    public void startWithData() {
        //PRAGMA foreign_keys = ON;
        LibraryCreator.createMediaEntityTable();
        LibraryCreator.createItemHistoryTable();
        LibraryCreator.createStudentsTable();
        libraryDao.insertAll(Arrays.asList(a1, b1, b2, dvd, cd, m));
        a1.setMediaId(1);
        b1.setMediaId(2);
        b2.setMediaId(3);
        dvd.setMediaId(4);
        cd.setMediaId(5);
        m.setMediaId(6);

        studentsDao.insert(s1);
        studentsDao.insert(s2);

        /*aHistory.setMediaId(1);
        bHistory.setMediaId(2);
        aHistory.setHistoryProperties(Date.valueOf("2020-01-08"), Date.valueOf("2020-02-07"), Date.valueOf("2020-02-05"), 4);
        bHistory.setHistoryProperties(Date.valueOf("2020-01-11"), Date.valueOf("2020-02-10"), Date.valueOf("2020-02-10"), 2);
        itemHistoryDao.insertAll(Arrays.asList(aHistory, bHistory));*/
    }

    @After
    public void cleanUpData() {
        LibraryCreator.dropTable_mediaEntity();
        //LibraryCreator.dropTable_itemHistory();
        LibraryCreator.dropTable_students();
    }

    @Test
    public void test_executeSelect() {
        Assert.assertEquals(Optional.of(b2), libraryDao.get(3));
        Assert.assertEquals(Optional.of(a1), libraryDao.get(1));
        Assert.assertEquals(Optional.of(b1), libraryDao.get(2));
        Assert.assertEquals(Optional.of(dvd), libraryDao.get(4));
        Assert.assertEquals(Optional.of(cd), libraryDao.get(5));
        Assert.assertEquals(Optional.empty(), libraryDao.get(10));
        Assert.assertEquals(Arrays.asList(a1, b1, b2, dvd, cd, m), libraryDao.getAll());

        libraryDao.delete(3);
        Assert.assertEquals(Optional.empty(), libraryDao.get(3));

        Magazine anotherItem = new Magazine("The Librarian", Date.valueOf("2019-10-19"), "Humanitas");
        libraryDao.insert(anotherItem);
        anotherItem.setMediaId(7);
        Assert.assertEquals(Optional.of(anotherItem), libraryDao.get(7));
        Assert.assertEquals(Optional.of(anotherItem), libraryDao.selectByTitle("The Librarian"));

    }

    @Test
    public void test_selectFromItemHistory() {
        Assert.assertEquals(Arrays.asList(aHistory), itemHistoryDao.selectFromHistoryByID(1));
    }

    @Test
    public void test_executeUpdate() {
        a1.setState(ItemState.BORROWED);
        a1.setStartDate(Date.valueOf("2020-01-11"));
        a1.setDueDate(Date.valueOf("2020-02-11"));
        a1.setStudentId(10);
        libraryDao.update(a1);
        libraryDao.updateState(a1);

        Assert.assertEquals(Optional.of(a1), libraryDao.get(1));

        Article anotherArticle = new Article(a1.getTitle(), "abc", a1.getSource());
        anotherArticle.setMediaId(1);
        anotherArticle.setState(ItemState.BORROWED);
        anotherArticle.setStartDate(Date.valueOf("2020-01-11"));
        anotherArticle.setDueDate(Date.valueOf("2020-02-11"));
        anotherArticle.setStudentId(10);
        libraryDao.update(anotherArticle);
        Assert.assertEquals(Optional.of(anotherArticle), libraryDao.get(1));

        anotherArticle.setState(ItemState.LOST);
        libraryDao.update(anotherArticle);
        Assert.assertEquals(Optional.of(anotherArticle), libraryDao.get(1));
    }

    @Test
    public void test_UpdateState() {
        a1.setState(ItemState.BORROWED);
        a1.setStudentId(2);
        a1.setStartDate(Date.valueOf("2020-01-20"));
        a1.setDueDate(Date.valueOf("2020-02-10"));
        a1.setReturnDate(Date.valueOf("2020-02-13"));
        libraryDao.updateState(a1);

        Article correctArticleUpdate = new Article("Active metabolites of some exotic fruits", "Hannah M.", "ScienceDirect");
        correctArticleUpdate.setMediaId(1);
        correctArticleUpdate.setState(ItemState.BORROWED);
        correctArticleUpdate.setStudentId(2);
        correctArticleUpdate.setStartDate(Date.valueOf("2020-01-20"));
        correctArticleUpdate.setDueDate(Date.valueOf("2020-02-10"));
        Assert.assertEquals(Optional.of(correctArticleUpdate), libraryDao.get(1));

        a1.setState(ItemState.AVAILABLE);
        libraryDao.updateState(a1);
        correctArticleUpdate.setState(ItemState.AVAILABLE);
        correctArticleUpdate.setStudentId(0);
        correctArticleUpdate.setStartDate(null);
        correctArticleUpdate.setDueDate(null);
        Assert.assertEquals(Optional.of(correctArticleUpdate), libraryDao.get(1));

        a1.setState(ItemState.BORROWED);
        libraryDao.updateState(a1);
        correctArticleUpdate.setState(ItemState.BORROWED);
        correctArticleUpdate.setStudentId(2);
        correctArticleUpdate.setStartDate(Date.valueOf("2020-01-20"));
        correctArticleUpdate.setDueDate(Date.valueOf("2020-02-10"));
        Assert.assertEquals(Optional.of(correctArticleUpdate), libraryDao.get(1));

        a1.setState(ItemState.LOST);
        libraryDao.updateState(a1);
        correctArticleUpdate.setState(ItemState.LOST);
        Assert.assertEquals(Optional.of(correctArticleUpdate), libraryDao.get(1));

        a1.setState(ItemState.AVAILABLE);
        libraryDao.updateState(a1);
        Assert.assertEquals(Optional.of(correctArticleUpdate), libraryDao.get(1));
    }

    @Test
    public void test_selectByTitle() {
        Assert.assertEquals(Optional.of(a1), libraryDao.selectByTitle("Active metabolites of some exotic fruits"));
        Assert.assertEquals(Optional.of(b1), libraryDao.selectByTitle("In cautarea fericirii"));
        Assert.assertEquals(Optional.of(cd), libraryDao.selectByTitle("abc"));
        Assert.assertEquals(Optional.of(dvd), libraryDao.selectByTitle("The thoughts that once we had"));
        Assert.assertEquals(Optional.of(m), libraryDao.selectByTitle("The New York Times"));
        Assert.assertEquals(Optional.empty(), libraryDao.selectByTitle("New York"));
        Assert.assertEquals(Optional.empty(), libraryDao.selectByTitle("THE NEW YORK TIMES"));
    }

    @Test
    public void test_selectByState() {
        Assert.assertEquals(Arrays.asList(a1, b1, b2, dvd, cd, m), libraryDao.selectByState(ItemState.AVAILABLE));
        Assert.assertEquals(Arrays.asList(), libraryDao.selectByState(ItemState.BORROWED));
    }
}