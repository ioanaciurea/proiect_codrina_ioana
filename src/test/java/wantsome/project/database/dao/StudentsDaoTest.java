package wantsome.project.database.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import wantsome.project.database.ddl.LibraryCreator;
import wantsome.project.database.dto.library.Student;

import java.util.Arrays;
import java.util.Optional;

public class StudentsDaoTest {
    Student s1 = new Student(1234567, "Ion Popescu", "ion@gmail.com");
    Student s2 = new Student(1000, "Marian Popescu", "marian@gmail.com");
    Student s3 = new Student(4536, "Andreea", "andreea123@gmail.com");

    StudentsDao studentsDao = new StudentsDao();

    @Before
    public void startWithData() {
        LibraryCreator.createStudentsTable();
        studentsDao.insert(s1);
        studentsDao.insert(s2);
        studentsDao.insert(s3);
    }

    @After
    public void cleanUpData() {
        LibraryCreator.dropTable_students();
    }

    @Test
    public void test_select() {
        Assert.assertEquals(Arrays.asList(s2, s3, s1), studentsDao.selectAll());
        Assert.assertEquals(Arrays.asList(s2), studentsDao.select("Mari"));
        Assert.assertEquals(Arrays.asList(s2), studentsDao.select("mari"));
        Assert.assertEquals(Arrays.asList(s2, s1), studentsDao.select("popescu"));

        Student anotherStudent = new Student(34707, "Mihai Popescu", "mpopescu@gmail.com");
        studentsDao.insert(anotherStudent);
        Assert.assertEquals(Arrays.asList(s2, anotherStudent, s1), studentsDao.select("popescu"));
    }

    @Test
    public void test_selectById() {
        Assert.assertEquals(Optional.empty(), studentsDao.selectById("0000"));
        Assert.assertEquals(Optional.empty(), studentsDao.selectById("000000003456"));

        Assert.assertEquals(Optional.of(s1), studentsDao.selectById("1234567"));
        Assert.assertEquals(Optional.of(s2), studentsDao.selectById("1000"));
        Assert.assertEquals(Optional.of(s3), studentsDao.selectById("4536"));
    }
}
