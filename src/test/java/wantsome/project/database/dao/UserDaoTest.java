package wantsome.project.database.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mindrot.jbcrypt.BCrypt;
import wantsome.project.database.ddl.UsersCreator;
import wantsome.project.database.dto.users.User;
import wantsome.project.database.dto.users.UserType;

import java.util.Arrays;
import java.util.Optional;

public class UserDaoTest {
    public String salt = BCrypt.gensalt();
    User user = new User("login1", salt, BCrypt.hashpw("1234", salt), "codrina", "aaa@gmail.com", UserType.ADMINISTRATOR);
    User user1 = new User("login2", salt, BCrypt.hashpw("1234", salt), "codrina1", "bbb@gmail.com", UserType.ADMINISTRATOR);
    User user2 = new User("login3", salt, BCrypt.hashpw("1234", salt), "codrina2", "CCC@gmail.com", UserType.LIBRARIAN);

    UserDao userDao = new UserDao();

    @Before
    public void startWithData() {
        UsersCreator.createUsersTable();
        userDao.insertAll(Arrays.asList(user, user1, user2));
        user.setId(1);
        user1.setId(2);
        user2.setId(3);
    }

    @After
    public void cleanUpData() {
        UsersCreator.dropUsersTable();
    }

    @Test
    public void test_executeSelect() {
        //SELECT by ID
        Assert.assertEquals(Optional.of(user1), userDao.get(2));
        Assert.assertEquals(Optional.of(user), userDao.get(1));
        Assert.assertEquals(Optional.of(user2), userDao.get(3));
        Assert.assertEquals(Optional.empty(), userDao.get(10));
        //SELECT all
        Assert.assertEquals(Arrays.asList(user, user1, user2), userDao.getAll());
        //delete one item
        userDao.delete(2);
        Assert.assertEquals(Arrays.asList(user, user2), userDao.getAll());
        //delete all items
        userDao.deleteAll();
        Assert.assertEquals(Arrays.asList(), userDao.getAll());

        //INSERT new item
        User anotherUser = new User("login1", salt, BCrypt.hashpw("1234", salt), "codrina", "aaa@gmail.com", UserType.ADMINISTRATOR);
        userDao.insert(anotherUser);
        anotherUser.setId(4);
        Assert.assertEquals(Optional.of(anotherUser), userDao.get(4));
    }

    @Test
    public void test_selectByLoginName() {
        Assert.assertEquals(Optional.of(user), userDao.getByLoginName("login1"));
        Assert.assertEquals(Optional.of(user2), userDao.getByLoginName("login3"));
        Assert.assertEquals(Optional.empty(), userDao.getByLoginName("login10"));
    }

    /*@Test
    public void test_selectByCredentials() {
        Assert.assertEquals(Optional.of(user), userDao.getByCredentials("login1", "1234"));
        Assert.assertEquals(Optional.of(user2), userDao.getByCredentials("login3", "1234"));
        Assert.assertEquals(Optional.empty(), userDao.getByCredentials("login10", "1234"));
        Assert.assertEquals(Optional.empty(), userDao.getByCredentials("login1", "abc?"));
        //uppercase letters in LOGIN_NAME
        Assert.assertEquals(Optional.empty(), userDao.getByCredentials("LOGIN1", "1234"));
    }*/


    /*@Test
    public void test_executeUpdate() {
        userDao.update(new User(user1.getLoginName(), salt, BCrypt.hashpw("0000", salt), user1.getRealName(), user1.getEmail(), user1.getType()));
        user1.setPassword("0000");
        Assert.assertEquals(Optional.of(user1), userDao.get(2));
        Assert.assertEquals(Optional.of(user1), userDao.getByLoginName("login2"));
        Assert.assertEquals(Optional.of(user1), userDao.getByCredentials("login2", "0000"));
    }*/
}